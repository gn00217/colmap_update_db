# Instructions - To reconstruct using your Cam Params

In Colmap Gui create a new project and run feat extraction. 
Update the database with your camera params using database_update provide the images.txt cameras.txt and your database.db using these flag --images_file --cameras_file --database_path. 
Go back into the Gui and edit your existing db, run feature matching and then start reconstruction.
Once finished you can export model as txt.

